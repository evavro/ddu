/* DDU - A D implementation of linux's du
 * 
 * By: Erik Vavro
 * 
 * Operating System Concepts - Fall 2011
 *
 * ======================================
 * 
 *	-a: Average file size
 *	-o N: Oldest last modified entires
 *	-y N: Newest last modified entries
 *	-l N: Largest entry in directory
 *	-s N: Smallest entry in directory
 *	-h: human readable format
 * 	-save file: Save the output of the traversal to a log file (PARTIALLY IMPLEMENTED)
 * 	-comp file: Compare the results of this traversal to a past traversal (PARTIALLY IMPLEMENTED)
 */

import std.stdio;
import std.c.math;
import std.cstream;
import std.string;
import std.conv;
import std.file;
import std.datetime;
 
// Global variables
private
{
	string compiledName;
	string dirPath = ".";
	
	Command[int] validCommands;
	Command[int] flaggedCommands;
	
	DirEntry[hash_t] dirScanLog;
	hash_t[int] dirScanLogOrder;
}

// Represents a valid command (to potentially be run)
class Command
{
	private
	{
		char[] flag;
		char[] flagVal;
		void function() action;
		Command precessor;
		bool wasRan;
		bool needFlagVal;
	}
	
	this(string flag, void function() action, bool needFlagVal)
	{
		this.flag = cast(char[]) flag;
		this.action = action;
		this.needFlagVal = needFlagVal;
	}
	
	// Find a matching valid command
	static Command findValid(char[] command)
	{
		return findCommand(command, validCommands);
	}
	
	// Find a matching flagged command
	static Command findFlagged(char[] command)
	{
		return findCommand(command, flaggedCommands);
	}
	
	static Command findCommand(char[] command, Command[int] list)
	{
		Command found = null;
		
		foreach(int i, Command c; list)
		{
			if(c.flag == command)
				found = c;
		}
		
		return found;
	}
	
	bool hasFlagVal()
	{
		return flagVal !is null;
	}
	
	bool canRun()
	{
		return !needFlagVal || (needFlagVal && hasFlagVal());
	}
	
	// Dynamically calls the function that this command corresponds to
	void process()
	{
		if(canRun())
		{
			if(action !is null)
				action();
				
			wasRan = true;
		} else {
			writefln("\nError: Cannot process flag %s. You need to specify a value.", flag);
		}
	}
}

// * Command function literals

// The primary function; scans the provided directory and prints out the results
void function() c_main = function void()
{
	initScanDirectory(dirPath);
};

// Average file size of the scan
void function() c_a = function void()
{
	ulong runningAvg = 0;
	double average = 0.0;
	
	if(dirScanLog !is null)
	{
		foreach(ulong i, DirEntry dir; dirScanLog)
			runningAvg += dir.size;
		
		average = runningAvg / dirScanLog.length;

		writefln("\nAverage file size: %s", printBytes(cast(ulong) average));
	}
};

// Finds the N oldest files (Usage: -o N)
void function() c_o = function void()
{
	auto c = Command.findValid(cast(char[]) "-o");
	
	sortDirsByModTime(c);
};

// Finds the N youngest files (Usage: -y N)
void function() c_y = function void()
{
	auto c = Command.findValid(cast(char[]) "-y");
	
	sortDirsByModTime(c);
};

// Finds the N largest files (Usage: -l N)
void function() c_l = function void()
{
	auto c = Command.findValid(cast(char[]) "-l");
	
	sortDirsBySize(c);
};

// Finds the N smallest files (Usage: -s N)
void function() c_s = function void()
{
	auto c = Command.findValid(cast(char[]) "-s");
	
	sortDirsBySize(c);
};

// Save the resulting scan as a formatted file that can be used for comparisons later
void function() c_save = function void()
{
	auto c = Command.findValid(cast(char[]) "-save");
	auto filename = c.flagVal;
	auto f = std.stdio.File(cast(string) filename, "w");
	
	if(dirScanLog !is null)
	{
		// Format: hashed_filename file_size last_mod_time
		foreach(ulong i, DirEntry dir; dirScanLog)
			f.writeln(toHash(dir.name), " ", dir.size, " ", dir.timeStatusChanged().stdTime());
			
		writefln("\n[ Saved log in %s as %s ]", dirPath, filename);
	} else {
		writefln("\nError: Could not save an instance of directory scan because the scan contains no results!");
	}
};

// Compares the state of the current directory traversal and a past one (if possible)
// NOTE: I could not implement this in time! This is ~ 75% complete
void function() c_compare = function void()
{
	auto c = Command.findValid(cast(char[]) "-comp");
	auto filename = c.flagVal;
	
	if(dirScanLog !is null)
	{
		if(exists(filename) != 0)
		{
			auto f = std.stdio.File(cast(string) filename, "r");
			auto content = cast(char[]) read(filename);
			
			// Iterate through each line and determine if a difference has occured
			foreach(line; std.stdio.File(cast(string) filename).byLine())
			{
				auto splitLine = std.array.splitter(line);
				auto isKey = true;
				
				foreach(chunk; splitLine)
				{
					if(isKey) {
						//auto key = cast(hash_t) chunk;
						//dirScanLog[key];
						
						writefln("%s", chunk);
						
						isKey = false;
					}
				}
			}
		} else {
			writefln("\nError: Cannot find %s", filename);
		}
	} else {
		writefln("\nError: Cannot compare scan files because the scan contains no results!");
	}
};

// Initialize the valid list of commands
private void initValidCommands()
{
	validCommands[0] = new Command(compiledName, c_main, false);
	validCommands[1] = new Command("-a", c_a, false);
	validCommands[2] = new Command("-o", c_o, true);
	validCommands[3] = new Command("-y", c_y, true);
	validCommands[4] = new Command("-l", c_l, true);
	validCommands[5] = new Command("-s", c_s, true);
	validCommands[6] = new Command("-save", c_save, true);
	validCommands[7] = new Command("-comp", c_compare, true);
	validCommands[8] = new Command("-h", null, false);
}

private void initScanDirectory(string path)
{
	writefln("\nDirectory listing of %s", dirPath);
	writefln("\nBytes\t\tLast Modified\t\t\tDirectory\n------------------------------------------------------");
	
	scanDirectory(path);
}

// Recursively searches through a directory path (depth first search)
private ulong scanDirectory(string path)
{
	auto directoryEntries = dirEntries(path, SpanMode.shallow, false);
	auto currentDirectory = dirEntry(path);
	auto totalSize = currentDirectory.size();
	
	// Look at each entry in the current directory
	foreach(DirEntry e; directoryEntries)
	{
		// If there's a subdirectory
		if(e.isDir)
		{
			totalSize += scanDirectory(e.name);
		} else if (e.isFile) {
			totalSize += e.size;
		}
		
		auto eHash = toHash(e.name);
		
		// Log every file we run across, and the logical order
		dirScanLog[eHash] = e;
		
		// Links a directory entry hash to an index (for file comparator)
		dirScanLogOrder[cast(int) dirScanLog.length] = eHash;
	}

	writefln("%s\t\t%s\t\t%s", printBytes(totalSize), currentDirectory.timeStatusChanged(), path);
	
	return totalSize;
}

// Sorts the directory log by modification time based on the command flag
void sortDirsByModTime(Command c)
{
	auto flagVal = c.flagVal;
	auto n = 0, n_count = 0;
	
	DirEntry[ulong] timeLog;
	ulong[] sortedTimeLog;
	
	try
	{
		// Try to convert the flag value to an integer
		n = parse!int(flagVal);
		
		if(dirScanLog !is null)
		{
			// Create a time index map for dirScanLog
			foreach(ulong i, DirEntry dir; dirScanLog)
				timeLog[dir.timeStatusChanged().stdTime()] = dir;
			
			// Order the array by key
			sortedTimeLog = timeLog.keys;
			
			if(c.flag == "-o")
			{
				writefln("\n%s oldest entries in %s\n-------------------------", n, dirPath);
				
				sortedTimeLog.sort;
			} else if(c.flag == "-y") {
				writefln("\n%s youngest entries in %s\n-------------------------", n, dirPath);
				
				sortedTimeLog.sort.reverse;
			}
			
			foreach(ulong i; sortedTimeLog)
			{	
				// Stop looping once we have found the n oldest/youngest files or the length of the log
				if(n_count == n || n_count == dirScanLog.length)
					break;
					
				n_count++;
					
				writefln("%s: %s", cast(SysTime) i, timeLog[i].name);
			}
		}
	} catch (Exception e) {
		writefln("\nError: The flag value \"%s\" for %s is not an integer", flagVal, c.flag);
	}
}

// Sorts the directory log by size based on the command flag
// TODO: Incorporate into a template to get rid of redundant code (sortDirsByModTime), going to be tricky due to sorting limitations in D
void sortDirsBySize(Command c)
{
	auto flagVal = c.flagVal;
	auto n = 0, n_count = 0;
	
	ulong[ulong] dirSizeLog;
	ulong[] sortedDirSizeLog;
	
	try
	{
		// Try to convert the flag value to an integer
		n = parse!int(flagVal);
		
		if(dirScanLog !is null)
		{
			// Create an index map of dirScanLog
			foreach(ulong i, DirEntry dir; dirScanLog)
				dirSizeLog[dir.size] = i;
			
			// Order the array by key
			sortedDirSizeLog = dirSizeLog.keys;
			
			if(c.flag == "-s")
			{
				writefln("\n%s smallest entries in %s\n-------------------------", n, dirPath);
				
				sortedDirSizeLog.sort;
			} else if(c.flag == "-l") {
				writefln("\n%s largest entries in %s\n-------------------------", n, dirPath);
				
				sortedDirSizeLog.sort.reverse;
			}
			
			foreach(ulong i; sortedDirSizeLog)
			{	
				// Stop looping once we have found the n largest/smallest files or the length of the log
				if(n_count == n || n_count == dirScanLog.length)
					break;
					
				n_count++;
				
				writefln("%s \t\t%s", printBytes(cast(int) i), dirScanLog[dirSizeLog[i]].name);
			}
		}
	} catch(Exception e) {
		writefln("\nError: The flag value \"%s\" for %s is not an integer", flagVal, c.flag);
	}
}

// Prints out the size of a file based on the human readable (-h) flag
string printBytes(ulong size)
{
	auto c = Command.findFlagged(cast(char[]) "-h");
	auto s = cast(string) to!dstring(size);
	auto label = " bytes";
	auto sizeMod = 1;
	auto dSize = 0.0;
	
	// If -h is flagged, print sizes with byte label
	if(c !is null)
	{
		if(size > 1000)
		{
			sizeMod = 1000;
			label = " kb";
		}
		
		if(size > 1000 * 1000)
		{
			sizeMod = 1000 * 1000;
			label = " mb";
		}
		
		if(size > 1000 * 1000 * 1000)
		{
			sizeMod = 1000 * 1000 * 1000;
			label = " gb";
		}
		
		dSize = (cast(double) size) / sizeMod;
		
		// Convert to a string and append the label
		s = (cast(string) to!dstring(dSize)) ~ label;
	}
	
	return s;
}

// Creates a unique hash value for a string
hash_t toHash(string s)
{
	hash_t hash;
	
	foreach(char c; s)
		hash = (hash * 9) + c;
	
	return hash;
}

// Parses the command-line arguments into flags and flag values
void parseArgs(char[][] args)
{
	Command command;
	bool nextArgFlagVal = false;
	
	// Iterate through the commands and flags and determine if they're valid
	foreach(argc, argv; args)
    {
		command = !nextArgFlagVal ? Command.findValid(argv) : command;
			
		if(command !is null)
		{	
			// Log the flag (command) if it doesn't need an associated value or if we have just found the needed value
			if(!nextArgFlagVal)
				flaggedCommands[cast(int) flaggedCommands.length] = command;
			
			// We have encountered a required value for a flag
			if(command.needFlagVal && !command.hasFlagVal())
			{
				if(!nextArgFlagVal)
				{
					nextArgFlagVal = true;
				} else {
					
					// Once we find the flag value, set it to the current argument
					command.flagVal = argv;
					
					nextArgFlagVal = false;
				}
			}
		} else {
				
			// Only interpet the flag as an unknown if we aren't looking at a path
			if(argv[0] == '.' || argv[0] == '/')
			{
				dirPath = cast(string) argv;
			} else {
				writefln("\nError: Unrecognized flag %s\n", argv);
				
				throw new Exception(null);
			}
		}
    }
}

// Processes each command (flag) in the order they were entered
void processCommands()
{
	foreach(int i, Command c; flaggedCommands)
		c.process();
		
	writefln("");
}

// Starting point
void main(char[][] args)
{
	// Determine the compiled name of the application
	compiledName = cast(string) args[0];
	
	try
	{
		// Sets up the valid commands and associates them with an action
		initValidCommands();
		
		// Parse the command-line arguments
		parseArgs(args);
		
		// Process the commands and flags
		processCommands();
		
	} catch(Exception e) {
		return;
	}
}
